Centre Mersenne is supported by CNRS and Universite Grenoble-Alpes where it is hosted. It can offer (for free, but capacity may be limited) a customized OJS editorial software with IT support, DOIs, plagiarism checking, with other services such as typesetting offered a la carte. 

More details:

* [Mersenne site] (http://www.mersenne.fr/en/about-mersenne/)
* [Algebraic Combinatorics] (http://algebraic-combinatorics.org/)
