# [Free Journal Network](http://freejournals.org/)

## What is the Free Journal Network?

Our goal is to promote scholarly journals run according to the [Fair Open Access model](https://www.fairopenaccess.org/); 
roughly, journals that:

- are controlled by the scholarly community;
- have no financial barriers to readers and authors.

Such journals have a long history. Many of them are of high quality and prestige, for instance,
[Acta Mathematica](https://intlpress.com/site/pub/pages/journals/items/acta/_home/_main/index.html),
one of the most prestigious mathematics research journals in the world, 
providing fully open online access to its entire content from 1882.

We strive to help such journals to coordinate their efforts to accelerate the creation of a journal ecosystem that will eventually replace the 
commercially controlled journals. Such efforts are complementary to the work of discipline-based organizations such as 
[LingOA](https://www.lingoa.eu/), [MathOA](http://www.mathoa.org/), [PsyOA](http://psyoa.org/), and the 
overarching [FOAA](https://www.fairopenaccess.org/), that focus primarily on moving commercially controlled  journals to the Fair Open Access model.
